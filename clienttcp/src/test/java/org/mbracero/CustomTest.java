package org.mbracero;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mbracero.integration.gateway.SimpleGateway;
import org.mbracero.model.Item;
import org.mbracero.model.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.integration.ip.tcp.connection.MessageConvertingTcpMessageMapper;
import org.springframework.integration.ip.tcp.connection.TcpConnectionSupport;
import org.springframework.integration.ip.tcp.connection.TcpListener;
import org.springframework.integration.ip.tcp.connection.TcpNetConnection;
import org.springframework.integration.ip.tcp.serializer.MapJsonSerializer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.support.converter.MapMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring-integration-tcp-client-context.xml"})
public class CustomTest {
	
	@Autowired
	SimpleGateway simpleGateway;

	@Test
	public void callEcho() {
		try {
			String reply = simpleGateway.send("Hola mundo!!!");  
			System.out.println("Replied with: " + reply);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void callRequest() {
		try {
			Item item = new Item(1, "ManuelBraceroGonzalez");
			Result reply = simpleGateway.sendData(item);
			System.out.println("Replied with: " + reply);

			item = new Item(2, "Hola mundo cruel");
			reply = simpleGateway.sendData(item);
			System.out.println("Replied with: " + reply);
			
			item = new Item(3, "Hecho con spring integration");
			reply = simpleGateway.sendData(item);
			System.out.println("Replied with: " + reply);
			
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void callRequestWithHeader() {
		try {
			Item item = new Item(1, "ManuelBraceroGonzalez");
			Message message = MessageBuilder.withPayload(item)
					.setHeader("bar", "baz")
					.build();
			Result reply = simpleGateway.sendDataWithHeader(message);
			System.out.println("Replied with: " + reply);
			
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	private final ApplicationEventPublisher nullPublisher = new ApplicationEventPublisher() {
		public void publishEvent(ApplicationEvent event) {
		}
	};
	
	@Test
	public void transferHeaders() throws Exception {
		Socket inSocket = mock(Socket.class);
		PipedInputStream pipe = new PipedInputStream();
		when(inSocket.getInputStream()).thenReturn(pipe);

		TcpConnectionSupport inboundConnection = new TcpNetConnection(inSocket, true, false, nullPublisher, null);
		inboundConnection.setDeserializer(new MapJsonSerializer());
		MapMessageConverter inConverter = new MapMessageConverter();
		MessageConvertingTcpMessageMapper inMapper = new MessageConvertingTcpMessageMapper(inConverter);
		inboundConnection.setMapper(inMapper);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Socket outSocket = mock(Socket.class);
		TcpNetConnection outboundConnection = new TcpNetConnection(outSocket, true, false, nullPublisher, null);
		when(outSocket.getOutputStream()).thenReturn(baos);

		MapMessageConverter outConverter = new MapMessageConverter();
		outConverter.setHeaderNames("bar");
		MessageConvertingTcpMessageMapper outMapper = new MessageConvertingTcpMessageMapper(outConverter);
		outboundConnection.setMapper(outMapper);
		outboundConnection.setSerializer(new MapJsonSerializer());

		Message<String> message = MessageBuilder.withPayload("foo")
				.setHeader("bar", "baz")
				.build();
		outboundConnection.send(message);
		PipedOutputStream out = new PipedOutputStream(pipe);
		out.write(baos.toByteArray());
		out.close();

		final AtomicReference<Message<?>> inboundMessage = new AtomicReference<Message<?>>();
		TcpListener listener = new TcpListener() {

			public boolean onMessage(Message<?> message) {
				inboundMessage.set(message);
				return false;
			}
		};
		inboundConnection.registerListener(listener);
		inboundConnection.run();
		assertNotNull(inboundMessage.get());
		assertEquals("foo", inboundMessage.get().getPayload());
		assertEquals("baz", inboundMessage.get().getHeaders().get("bar"));
	}
	
}
