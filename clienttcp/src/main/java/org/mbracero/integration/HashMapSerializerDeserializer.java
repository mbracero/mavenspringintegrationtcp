package org.mbracero.integration;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.serializer.Deserializer;
import org.springframework.core.serializer.Serializer;

/**
 * https://github.com/spring-projects/spring-integration-samples/blob/master/basic/tcp-client-server/src/main/java/org/springframework/integration/samples/tcpclientserver/CustomSerializerDeserializer.java#L70
 *
 */

public class HashMapSerializerDeserializer implements Serializer<Map<?, ?>>, Deserializer<Map<?, ?>> {
	

	private static final int ORDER_NUMBER_LENGTH = 1;
	private static final int SENDER_NAME_LENGTH = 15;
	
	public void serialize(Map<?, ?> object, OutputStream outputStream)
			throws IOException {
		System.out.println("clienttcp - ItemSerializerDeserializer (serialize) - object :: " + object);
		Map<String, String> headers = (Map<String, String>) object.get("headers");
	}

	public Map<?, ?> deserialize(InputStream inputStream) throws IOException {
		System.out.println("clienttcp - ItemSerializerDeserializer (deserialize) - object :: " + inputStream);
		Map<String, Object> map = new HashMap<String, Object>();
		return map;
	}

}
