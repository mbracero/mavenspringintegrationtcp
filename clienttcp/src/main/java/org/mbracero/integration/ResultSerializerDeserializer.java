package org.mbracero.integration;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.mbracero.model.Result;
import org.springframework.core.serializer.Deserializer;
import org.springframework.core.serializer.Serializer;

/**
 * https://github.com/spring-projects/spring-integration-samples/blob/master/basic/tcp-client-server/src/main/java/org/springframework/integration/samples/tcpclientserver/CustomSerializerDeserializer.java#L70
 *
 */

public class ResultSerializerDeserializer implements Serializer<Result>, Deserializer<Result> {
	

	private static final int ORDER_NUMBER_LENGTH = 3;
	private static final int SENDER_NAME_LENGTH = 4;
	
	public void serialize(Result object, OutputStream outputStream)
			throws IOException {
		System.out.println("clienttcp - ResultSerializerDeserializer (serialize) - Object :: " + object);
		byte[] number = object.getCode().getBytes();
		outputStream.write(number);

		byte[] senderName = object.getDescription().getBytes();
		outputStream.write(senderName);

		outputStream.flush();
	}

	public Result deserialize(InputStream inputStream) throws IOException {
		System.out.println("clienttcp - ResultSerializerDeserializer (deserialize) - Inputstream :: " + inputStream);
		String orderNumber = parseOrderNumber(inputStream);
		String senderName = parseSenderName(inputStream);

		Result order = new Result(orderNumber, senderName);
		System.out.println("clienttcp - ResultSerializerDeserializer (deserialize) - result :: " + order);
		return order;
	}
	
	private String parseString(InputStream inputStream, int length) throws IOException {
		StringBuilder builder = new StringBuilder();

		int c;
		System.out.println("clienttcp - ResultSerializerDeserializer (parseString) INIT");
		for (int i = 0; i < length; ++i) {
			c = inputStream.read();
			System.out.println("clienttcp - ResultSerializerDeserializer (parseString) - c :: " + c);
			checkClosure(c);
			builder.append((char)c);
		}
		
		System.out.println("clienttcp - ResultSerializerDeserializer (parseString) END");

		return builder.toString();
	}
	
	private String parseSenderName(InputStream inputStream) throws IOException {
		return parseString(inputStream, SENDER_NAME_LENGTH);
	}

	private String parseOrderNumber(InputStream inputStream) throws IOException {
		return parseString(inputStream, ORDER_NUMBER_LENGTH);
	}
	
	/**
	 * Check whether the byte passed in is the "closed socket" byte
	 * Note, I put this in here just as an example, but you could just extend the
	 * {@link org.springframework.integration.ip.tcp.serializer.AbstractByteArraySerializer} class
	 * which has this method
	 *
	 * @param bite
	 * @throws IOException
	 */
	protected void checkClosure(int bite) throws IOException {
		if (bite < 0) {
			throw new IOException("Socket closed during message assembly (client - result) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
	}

}
