package org.mbracero.integration.gateway;

import org.mbracero.model.Item;
import org.mbracero.model.Result;
import org.springframework.integration.annotation.Gateway;
import org.springframework.messaging.Message;

public interface SimpleGateway {
	@Gateway(requestChannel="input")
	public String send(String text);
	
	@Gateway(requestChannel="requestData")
	Result sendData(Item item);
	
	@SuppressWarnings("rawtypes")
	@Gateway(requestChannel="requestDataWithHeader")
	Result sendDataWithHeader(Message item);

}