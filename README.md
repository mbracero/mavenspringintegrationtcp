## mavenSpringIntegrationTcp

**Spring4 + maven + spring integration (tcp) **

Proyecto prueba de consumo de servicio TCP con Spring Integration

Generacion de proyecto 'padre' (desde directorio de workspace)

`mvn archetype:generate`

Cambiamos el paquete (packaging) del pom.xml de jar a pom.

**server:** (desde directorio 'padre')

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=servertcp -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false`

**client:** (desde directorio 'padre')

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=clienttcp -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false`

Para correr el servidor:

`mvn -pl servertcp/ jetty:run`


