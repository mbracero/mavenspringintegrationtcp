package org.mbracero.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ItemController {
	@RequestMapping(value="/api/echo", method=RequestMethod.GET)
	public @ResponseBody String getEcho() {
		String ret = "Hola mundo!!!!!!";
		System.out.println("Peticion /api/echo :: " + ret);
		return ret;
	}
	
}