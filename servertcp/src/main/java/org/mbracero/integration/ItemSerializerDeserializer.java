package org.mbracero.integration;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.mbracero.model.Item;
import org.springframework.core.serializer.Deserializer;
import org.springframework.core.serializer.Serializer;

/**
 * https://github.com/spring-projects/spring-integration-samples/blob/master/basic/tcp-client-server/src/main/java/org/springframework/integration/samples/tcpclientserver/CustomSerializerDeserializer.java#L70
 *
 */

public class ItemSerializerDeserializer implements Serializer<Item>, Deserializer<Item> {
	

	private static final int ORDER_NUMBER_LENGTH = 1;
	private static final int SENDER_NAME_LENGTH = 15;
	
	public void serialize(Item object, OutputStream outputStream)
			throws IOException {
		System.out.println("servertcp - ItemSerializerDeserializer (serialize) - object :: " + object);
		byte[] number = Integer.toString(object.getId()).getBytes();
		outputStream.write(number);

		byte[] senderName = object.getName().getBytes();
		outputStream.write(senderName);

		outputStream.flush();
	}

	public Item deserialize(InputStream inputStream) throws IOException {
		System.out.println("servertcp - ItemSerializerDeserializer (deserialize) - object :: " + inputStream);
		int orderNumber = parseOrderNumber(inputStream);
		String senderName = parseSenderName(inputStream);

		Item order = new Item(orderNumber, senderName);
		return order;
	}
	
	private String parseString(InputStream inputStream, int length) throws IOException {
		StringBuilder builder = new StringBuilder();

		int c;
		for (int i = 0; i < length; ++i) {
			c = inputStream.read();
			checkClosure(c);
			builder.append((char)c);
		}

		return builder.toString();
	}
	
	private String parseSenderName(InputStream inputStream) throws IOException {
		return parseString(inputStream, SENDER_NAME_LENGTH);
	}

	private int parseOrderNumber(InputStream inputStream) throws IOException {
		String value = parseString(inputStream, ORDER_NUMBER_LENGTH);
		return Integer.valueOf(value.toString());
	}
	
	/**
	 * Check whether the byte passed in is the "closed socket" byte
	 * Note, I put this in here just as an example, but you could just extend the
	 * {@link org.springframework.integration.ip.tcp.serializer.AbstractByteArraySerializer} class
	 * which has this method
	 *
	 * @param bite
	 * @throws IOException
	 */
	protected void checkClosure(int bite) throws IOException {
		if (bite < 0) {
			throw new IOException("Socket closed during message assembly (server - item) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
	}

}
