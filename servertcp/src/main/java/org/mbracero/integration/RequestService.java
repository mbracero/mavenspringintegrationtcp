package org.mbracero.integration;

import org.mbracero.model.Item;
import org.mbracero.model.Result;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

@Service
public class RequestService {
	
	@ServiceActivator(inputChannel="channelServerRequestData", outputChannel="channelServerResponseData")
	public Result requestData(Item input) {
		System.out.println("Input :::: " + input);
		Result ret = new Result("AAA", "DDDD");
		System.out.println("Ret :::: " + ret);
		return ret;
	}

}
