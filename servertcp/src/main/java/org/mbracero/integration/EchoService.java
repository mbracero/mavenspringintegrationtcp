package org.mbracero.integration;

import org.mbracero.model.Item;
import org.mbracero.model.Result;

public class EchoService {

	public String test(String input) {
		System.out.println("Input :::: " + input);
		/*
		try {
			Thread.sleep(5000); // Timeout control
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		if ("FAIL".equals(input)) {
			throw new RuntimeException("Failure Demonstration");
		}
		return "echo:" + input;
	}
	
	public Result requestData(Item input) {
		System.out.println("Input :::: " + input);
		Result ret = new Result("AAA", "DDDD");
		System.out.println("Ret :::: " + ret);
		return ret;
	}

}
