package org.mbracero.controller;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.mbracero.model.Item;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ItemController {
	@SuppressWarnings("serial")
	private List<Item> items = new CopyOnWriteArrayList<Item>() {{
		add(new Item(1l, "item 1"));
		add(new Item(2l, "item 2"));
		add(new Item(3l, "item 3"));
		add(new Item(4l, "item 4"));
		add(new Item(5l, "item 5"));
		add(new Item(6l, "item 6"));
	}};
	
	@RequestMapping(value="/api/echo", method=RequestMethod.GET)
	public @ResponseBody String getEcho() {
		String ret = "Hola mundo!!!!!!";
		/*
		try {
			Thread.sleep(3000); // Timeout control
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		System.out.println("Peticion /api/echo :: " + ret);
		return ret;
	}
	
	@RequestMapping(value="/api/items", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItemsDefault() {
		System.out.println("Peticion /api/items :: " + items.toString());
		return items;
	}
	
	@RequestMapping(value="/api/items2", method=RequestMethod.GET)
	public @ResponseBody Item getItemsDefault2() {
		Item item = items.get(0);
		System.out.println("Peticion /api/items2 :: " + item);
		return item;
	}
	
	@RequestMapping(value="/api/item/{id}", method=RequestMethod.GET)
	public @ResponseBody Item getItemsDefault(@PathVariable(value="id") long id, @RequestParam(value="name") String name) {
		Item ret = null;
		for (Item item : items) {
			if (item.getId().equals(id)) {
				ret = item;
			}
		}
		/*
		try {
			Thread.sleep(3000); // Timeout control
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		System.out.println("Peticion /api/items/{id} :: id = " + id + ", name = " + name + " :: " + ret);
		return ret;
	}
	
}